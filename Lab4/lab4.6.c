#include<stdio.h>
#include<stdlib.h>
int ackermann(int x, int y);

int main()
{
	int x,y;
	printf("Please Enter two non-negative integers\n");
	scanf("%d %d", &x, &y);

	printf("\nAckermann Function with inputs (%d,%d) is %d\n",x,y,ackermann(x,y));
}

int ackermann(int x, int y)
{
	if(x==0)
		return y+1;
	else if(y==0)
		return ackermann(x-1,1);
	else 
		return ackermann(x-1,ackermann(x,y-1));
}
