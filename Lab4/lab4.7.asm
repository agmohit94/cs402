	.data
msg1:	.asciiz "Please enter two non-negative integer numbers: "
msg2:	.asciiz "One or more of the integers entered are negative. Please re-enter both: "
msg3:	.asciiz "Output value is:"

	.text
	.globl main
main:	sub $sp, $sp, 4
	sw $ra, 4($sp)
	
	li $v0, 4 			# system call for print_str
	la $a0, msg1 			# address of string to print
	syscall
	j check

Neg:	li $v0, 4 			# system call for print_str
	la $a0, msg2 			# address of string to print
	syscall
	
check:	li $v0, 5 			# system call for read_int
	syscall 			# the integer placed in $v0
	addu $t0, $v0, $0 		# move the number in $t0
	
	li $v0, 5 			# system call for read_int
	syscall 			# the integer placed in $v0
	addu $t1, $v0, $0 		# move the number in $t1
	
	bltz $t0, Neg
	bltz $t1, Neg

	add $a0, $t0, $0
	add $a1, $t1, $0
	jal Ackermann

	move $t2, $v0 
	
	li $v0, 4 			# system call for print_str
	la $a0, msg3 			# address of string to print
	syscall
	
	li $v0, 1
	addu $a0, $t2, $0
	syscall	
	
	lw $ra, 4($sp)
	add $sp, $sp, 4
	jr $ra

.text
Ackermann:	sub $sp, $sp, 8
		sw $s0, 8($sp)		# save the return address on stack
		sw $ra, 4($sp)

		beqz $a0, case_A	# case A
		beqz $a1, case_B	# case B
		move $s0, $a0
		sub $a1, $a1, 1		
		jal Ackermann		# after the termination condition is reached these lines will be executed

		move $a1, $v0
		sub $a0, $s0, 1	
		jal Ackermann

End:		lw $ra, 4($sp)
		lw $s0, 8($sp)
		add $sp, $sp, 8
		jr $ra

case_A:		
		add $v0, $a1, 1
		lw $ra, 4($sp)
		j End

case_B:		li $a1, 1
		sub $a0, $a0, 1
		jal Ackermann
		j End


	
