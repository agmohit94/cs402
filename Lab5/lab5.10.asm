		.data 0x10010000
		.align 0

ch1:	.byte 'a'
word1: 	.word 0x89abcdef
ch2:	.byte 'b'
word2:	.word 0
	
		.text
		.globl main

main:	
		lui $a0, 4097
		ori $s0, $a0, 1
		
		lwr $t0, 0($s0)
		add $a0, $a0, $0
		
		lwl $t1, 3($s0)
		add $a0, $a0, $0
		
		or $t2, $t0, $t1
		
		add $s1, $s0, 5
		swr $t2, 0($s1)
		swl $t2, 3($s1)
		
		jr $ra	
		add $a0, $a0, $0



