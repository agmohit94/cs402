		.data 0x10010000
		.align 0
		
char1:	.byte 'a'
double1: .double 1.1
char2:	.byte 'b' 
half1:	.half 0x8001
char3:	.byte 'c'
word1: 	.word 0x56789abc
char4: 	.byte 'd'

		.text
		.globl main
main:	
		lui $a0, 4097
		ori $s1, $a0, 13
	
		lwl $t0, 3($s1)
		lwr $t1, 0($s1)
	
		or $t2, $t0, $t1
		jr $ra	


