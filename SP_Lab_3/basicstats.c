/*=============================================================================
 | Author:  Mohit Aggarwal
 | Date:  01/May/19
 | Assigment: SP Lab 3
 | Description: This code reads data from a file and computes mean, median, standard deviation and the no. of input values.
 |		It also gives the unused space used in the array to store the values.
 +-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

float *read_file(int *size, int *cap, FILE *fp);
void sort_array(float *arr, int length);
float cal_mean(float *arr, int *size);
float cal_median(float *arr, int *size);
float cal_std(float *arr, int *size);

int main(int argc, char *argv[])
{

    float *my_array = NULL;
    int size, cap;

    FILE *fp = fopen(argv[1], "r");

    // If the file doesn't exist or unable to open, it prints the below message and exits the program.
    if (fp == NULL)
    {
        printf("Unable to Open file. Please try again.\n");
    }

    // call read_file function
    my_array = read_file(&size, &cap, fp);

    //Sorth the array to be able to calculate the median
    sort_array(my_array, size);

    printf("Results:\n");
    printf("--------\n");
    printf("Num values: %d\n", size);
    printf("      mean: %.3f\n", cal_mean(my_array, &size));
    printf("    median: %.3f\n", cal_median(my_array, &size));
    printf("    stddev: %.3f\n", cal_std(my_array, &size));
    printf("Unused array capacity: %d\n", (cap - size));
    free(my_array);
    return 0;
}

/*This function reads in values from the input file into a dynamically
  allocated array of floats.  It uses a doubling algorithm to increase
  the size of the array as it needs more space.

  Input: size pointer to the number of values read in from the file
         cap pointer to the total capacity of the array
         fp - file pointer to the input file
  
  Return Value: the base address of the array of values read in
*/

float *
read_file(int *size, int *cap, FILE *fp)
{

    float *orig_arr;
    float val;
    *cap = 20;
    orig_arr = (float *)malloc(*cap * sizeof(float));
    *size = 0;

    while (fscanf(fp, "%f", &val) != -1)
    {

        // Resizing the array to double the capacity in case it is fully filled
        if (*size >= *cap)
        {
            float *new_arr;
            *cap = (*cap) * 2;
            new_arr = (float *)malloc(*cap * sizeof(float));
            //Copy the values in old array to the new one
            for (int j = 0; j < *size; j++)
            {
                new_arr[j] = orig_arr[j];
            }
            free(orig_arr);
            orig_arr = new_arr;
        }
        orig_arr[*size] = val;
        *size += 1;
    }

    //close the file
    fclose(fp);

    return orig_arr;
}

/*This function sorts the array using selection sort

  Input: array and it's size
*/
void sort_array(float *arr, int size)
{
    int i;
    int j;
    for (i = 0; i < size - 1; i++)
    { //Iterate through array except last element
        int min = i;
        // Look for minimum val in idx range [i, len(arr))
        for (j = i + 1; j < size; j++)
        {
            // Maintain loop invariant: keep idx of min element in min_idx
            if (arr[j] < arr[min])
            {
                min = j;
            }
        }
        float temp = arr[i];
        arr[i] = arr[min];
        arr[min] = temp;
    }
}

//This function calculates the mean and return it
float cal_mean(float *arr, int *size)
{
    float sum = 0.0;
    for (int i = 0; i < *size; i++)
    {
        sum += arr[i];
    }
    return (sum / *size);
}

//This function calculates the median and return it
float cal_median(float *arr, int *size)
{
    int mid = *size / 2;

    if (*size % 2 == 0)
    {
        return 0.5 * (arr[mid] + arr[mid - 1]);
    }
    else
    {
        return arr[mid];
    }
}

//This function calculates the standard deviation and return it
float cal_std(float *arr, int *size)
{
    float sum = 0.0, mean = 0.0, variance = 0.0;
    for (int i = 0; i < *size; i++)
    {
        sum += arr[i];
    }
    mean = (sum / *size);
    for (int j = 0; j < *size; j++)
    {
        variance += (arr[j] - mean) * (arr[j] - mean);
    }
    variance /= *size;
    return sqrt(variance);
}
