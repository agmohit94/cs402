/*=============================================================================
 | Author:  Mohit Aggarwal
 | Date:  24/Apr/19
 | Assigment: SP Lab 2
 | Description: This code contains the definitions of the I/O system used 
 | 		in the main program
 +-----------------------------------------------------------------------------*/

#include "readfile.h"

FILE *Temp;

int open_file(char name[])
{

    fp = fopen(name, "r");

    if (fp == NULL)
    {
        return -1;
    }
    else
    {
        return 0;
    }
}

int close_file(FILE *f)
{

    int ret = fclose(f);

    if (ret == 1)
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

int read_int(int *a)
{
    if (fscanf(fp, " %d", a) == 1)
    {
        return 0;
    }
    else
        return -1;
}

int read_float(float *a)
{
    if (fscanf(fp, " %f", a) == 1)
    {
        return 0;
    }
    else
        return -1;
}

int read_string(char *a)
{
    if (fscanf(fp, " %s", a) == 1)
    {
        return 0;
    }
    else
        return -1;
}

void sortbyID(struct Employee arr[], int n)
{
    int j, i;

    for (i = 1; i < n; i++)
    {
        for (j = 0; j < n - i; j++)
        {
            if (arr[j].id > arr[j + 1].id)
            {
                struct Employee temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
}
