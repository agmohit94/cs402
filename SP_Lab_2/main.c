/*=============================================================================
 | Author:  Mohit Aggarwal
 | Date:  24/Apr/19
 | Assigment: SP Lab 2
 | Description: This code contains the code which will be used to manage the 
 |		employee information of a company. It gives option to:
 |		1. Print the database
 |		2. Lookup an employee by ID
 |		3. Lookup an employee by last name
 |		4. Add a new employee to the database
 |		5. Quit
 |   		6. Remove an employee
 |   		7. Update employee Information
 |   		8. Print top M employees with the highest salary
 |   		9. Print all employees with same Last Name
 +-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "readfile.h"
#include <stdlib.h>
#include <ctype.h>

int i = -1; //This will be used to keep track of employee count

struct Employee e[1024]; //An array of Employee struct to store the information of employees.

//This function is used to print the user menu.
//Input - None
//Output - N/A
void menu_display()
{
    printf("Employee DB Menu: \n");
    printf("---------------------------------- \n");
    printf("(1) Print the Database \n");
    printf("(2) Lookup by ID \n");
    printf("(3) Lookup by Last Name (only 1 employee will be shown) \n");
    printf("(4) Add an Employee \n");
    printf("(5) Quit \n");
    printf("(6) Remove an employee \n");
    printf("(7) Update employee Information \n");
    printf("(8) Print top M employees with the highest salary \n");
    printf("(9) Print all employees with same Last Name \n");
    printf("---------------------------------- \n");
    printf("Please enter your choice: ");
}

//This function is used to print the employee database
//Input - None
//Output - N/A
void print()
{
    printf("%-65s %-10s %-10s\n", "Name", "Salary", "ID");
    printf("---------------------------------------------------------------------------------------- \n");

    for (int k = 0; k < i; k++)
    {
        printf("%-32s %-32s %-10d %-10d \n", e[k].first, e[k].last, e[k].salary, e[k].id);
    }

    printf("---------------------------------------------------------------------------------------- \n");
    printf("No. of Employees = %d\n\n", i);
}

//This function is used to perform a binary search on the employee database on ID column.
//Input - The ID to searched for
//Output - N/A
int binarySearch(int x)
{
    int l = 0;
    int r = i;
    while (l <= r)
    {
        int m = l + (r - l) / 2;

        // Check if x is present at mid
        if (e[m].id == x)
        {
            printf("The employee with id %d is %s %s and has a salary of %d. \n \n", e[m].id, e[m].first, e[m].last, e[m].salary);
            return m;
        }

        // If x greater, ignore left half
        else if (e[m].id < x)
        {
            l = m + 1;
        }

        // If x is smaller, ignore right half
        else
        {
            r = m - 1;
        }
    }
    printf("Not found\n\n");
    // if we reach here, then element was not present
    return -1;
}

//This function is used to search for an employee from his last name.
//Input - Last name to be searched for
//Output - N/A
void lastNameSearch(char *a, int x)
{
    int temp = 0;
    for (int j = 0; j < i; j++)
    {
        if (strcasecmp(a, e[j].last) == 0)
        {
            if (temp == 0)
            {
                printf("%-65s %-10s %-10s\n", "Name", "Salary", "ID");
                printf("---------------------------------------------------------------------------------------- \n");
            }

            printf("%-32s %-32s %-10d %-10d \n", e[j].first, e[j].last, e[j].salary, e[j].id);
            temp = 1;
            if (x == 1)
            {
                break;
            }
        }
    }
    if (temp != 0)
    {

        printf("---------------------------------------------------------------------------------------- \n\n");
    }
    if (temp == 0)
    {
        printf("Not found\n\n");
    }
}

//This function is used to check whether the input for name of an employee is valid or not.
//Input - String Input from user
//Output - N/A
int checkNameLetters(char a[])
{

    int len = strlen(a);
    for (int p = 0; p < len; p++)
    {

        if (isalpha(a[p]) == 0)
        {
            return 0;
        }
    }

    return 1;
}

int checkSalaryDigit(char a[])
{
    int len = strlen(a);

    for (int q = 0; a[q] != '\0'; q++)
    {
        if (isdigit(a[q]) == 0)
        {
            return 1;
        }
    }
    return 0;
}
//This function is used to add a new employee to the database and also genrate an ID greater than any currently existing ID
//Input - None
//Output - N/A
void add_new()
{
    struct Employee temp;
    int flag;
    char salary_char[100];
    printf("\nPlease enter the First name of the user: ");
    scanf("%s", temp.first);
    while (checkNameLetters(temp.first) != 1)
    {
        printf("\nInvalid input. Please only use letters ");
        scanf("%s", temp.first);
    }

    printf("\nPlease enter the Last name of the user: ");
    scanf("%s", temp.last);
    while (checkNameLetters(temp.last) != 1)
    {
        printf("\nInvalid input. Please only use letters: ");
        scanf("%s", temp.last);
    }

    printf("\nPlease enter the salary of the user (between 30,000 & 150,000): ");
    scanf("%s", salary_char);

    while (checkSalaryDigit(salary_char) == 1)
    {
        printf("\nInvalid input. Please try again (between 30,000 & 150,000): ");
        scanf("%s", salary_char);
    }

    printf("\n Do you want to add the following employee to the DB?");
    printf("\n %s %s, salary: %s", temp.first, temp.last, salary_char);
    printf("\n Enter 1 for yes, 0 for no: ");
    scanf("%d", &flag);
    if (flag == 1)
    {
        temp.id = e[i - 1].id + 1;
        ++i;
        e[i - 1] = temp;
        printf("Employee added\n\n");
    }
    else if (flag == 0)
    {
        printf("\nNo new employee added");
    }
    else
    {
        printf("\nIncorrect input.");
        printf("\nYou will be redirected to main menu without updating the database");
    }
    return;
}

void remove_employee(int x)
{

    int find = binarySearch(x);
    int choice;
    if (find != -1)
    {
        printf("Are you sure you want to remove the employee with above details.\n");
        printf("Press 1 for Yes and 0 for No: ");
        scanf("%d", &choice);
        if (choice == 1)
        {

            for (int k = find; k < i; k++)
            {
                e[k] = e[k + 1];
            }
            --i;
            printf("\nEmployee removed\n");
        }
        else if (choice == 0)
        {

            printf("\nNo employee removed\n");
        }
        else
        {
            printf("\nIncorrect input.");
            printf("\nYou will be redirected to main menu without updating the database\n");
        }
    }
    else
    {
        printf("\nNo employee removed\n");
    }
    return;
}

void update_employee(int x)
{
    int find = binarySearch(x);
    int choice;

    if (find != -1)
    {
        printf("Do you want to update the First Name.\n");
        printf("Press 1 for Yes and 0 for No: ");
        scanf("%d", &choice);
        if (choice == 1)
        {
            printf("Please Enter the new first name: ");
            scanf("%s", e[find].first);
        }

        printf("Do you want to update the last Name.\n");
        printf("Press 1 for Yes and 0 for No: ");
        scanf("%d", &choice);
        if (choice == 1)
        {
            printf("Please Enter the new last name: ");
            scanf("%s", e[find].last);
        }

        printf("Do you want to update the Salary.\n");
        printf("Press 1 for Yes and 0 for No: ");
        scanf("%d", &choice);
        if (choice == 1)
        {
            printf("Please Enter the new salary: ");
            scanf("%d", &e[find].salary);
        }
    }
}

//This function is used to sort the Employee array salary in decreasing order
//Input - Array of Employee Struct & size of the array
//Output - N/A
void sortbySalary(struct Employee arr[], int n)
{

    int p, q;

    for (p = 1; p < n; p++)
    {
        for (q = 0; q < n - p; q++)
        {
            if (arr[q].salary < arr[q + 1].salary)
            {
                struct Employee temp = arr[q];
                arr[q] = arr[q + 1];
                arr[q + 1] = temp;
            }
        }
    }
}

//This function is used to print M employees with highest salary
//Input - M
//Output - N/A
void top_M(int m)
{

    if (m > i)
    {
        printf("Invalid Input. Total no. of employees is only %d \n\n", i);
        return;
    }
    sortbySalary(e, i);

    printf("%-65s %-10s %-10s\n", "Name", "Salary", "ID");
    printf("---------------------------------------------------------------------------------------- \n");
    for (int k = 0; k < m; k++)
    {

        printf("%-32s %-32s %-10d %-10d \n", e[k].first, e[k].last, e[k].salary, e[k].id);
    }
    printf("---------------------------------------------------------------------------------------- \n");
    sortbyID(e, i);
}

int main(int argc, char *argv[])
{

    int x = open_file(argv[1]);
    //In case the system is not able to open the input file, the program will exit after informing the user for the same.
    if (x == -1)
    {
        printf("Unable to open file. Please try again\n");
        exit(1);
    }

    while (fgetc(fp) != EOF)
    {
        ++i;
        read_int(&e[i].id);
        read_string(e[i].first);
        read_string(e[i].last);
        read_int(&e[i].salary);
    }

    close_file(fp);

    sortbyID(e, i);

    int m;
    int choice;
    int id_search;
    char last_name_search[64];
    menu_display();
    scanf("%d", &choice);

    while (choice != 5)
    {
        switch (choice)
        {
        case 1:
            print();
            menu_display();
            scanf("%d", &choice);
            break;
        case 2:
            printf("Enter the ID to be searched for: ");
            scanf("%d", &id_search);
            binarySearch(id_search);
            menu_display();
            scanf("%d", &choice);
            break;
        case 3:
            printf("Enter the last name to be searched for: ");
            scanf("%s", last_name_search);
            lastNameSearch(last_name_search, 1);
            menu_display();
            scanf("%d", &choice);
            break;
        case 4:
            add_new();
            menu_display();
            scanf("%d", &choice);
            break;
        case 6:
            printf("Enter the employee ID which needs to be removed: ");
            scanf("%d", &id_search);
            remove_employee(id_search);
            menu_display();
            scanf("%d", &choice);
            break;
        case 7:
            printf("Enter the employee ID to update details: ");
            scanf("%d", &id_search);
            update_employee(id_search);
            menu_display();
            scanf("%d", &choice);
            break;
        case 8:
            printf("Enter M: ");
            scanf("%d", &m);
            top_M(m);
            menu_display();
            scanf("%d", &choice);
            break;
        case 9:
            printf("Enter the last name: ");
            scanf("%s", last_name_search);
            lastNameSearch(last_name_search, 0);
            menu_display();
            scanf("%d", &choice);
            break;
        default:
            printf("%d is not between 1 and 5, please try again: ", choice);
            scanf("%d", &choice);
            break;
        }
    }
    if (choice == 5)
    {
        printf("Thanks for using the application. \n");
    }
}

