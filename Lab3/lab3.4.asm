	.data 0x10010000
i:	.word 0
initial_value:	.word 5
limit: .word 10
my_array: .space 40

 	.text
	.globl main

main:	addu $s0, $ra, $0 # save $31 in $16
	lw $t0, i
	lw $t1, initial_value
	la $a1, my_array
	lw $t2, limit

Loop:	ble $t2, $t0, Exit
	sw $t1, 0($a1)
	addi $t1, $t1, 1
	addi $a1, $a1, 4
	addi $t0, $t0, 1	
	j Loop


Exit:	addu $ra, $0, $s0 
	jr $ra

