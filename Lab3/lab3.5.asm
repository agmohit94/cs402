	.data 0x10010000
msg1:	.asciiz "Please enter two integer numbers: "
msg2:	.asciiz "I'm far away"
msg3:	.asciiz "I'm nearby"
 	.text 0x00400024
	.globl main

main:	addu $s0, $ra, $0 # save $31 in $16

	li $v0, 4 # system call for print_str
	la $a0, msg1 # address of string to print
	syscall
	
	li $v0, 5 # system call for read_int
	syscall # the integer placed in $v0
	addu $t0, $v0, $0 # move the number in $t0
	
	li $v0, 5 # system call for read_int
	syscall # the integer placed in $v0
	addu $t1, $v0, $0 # move the number in $t0

	bne $t0, $t1, Ne
	j Far

Ne:	li $v0, 4 # system call for print_str
	la $a0, msg3 # address of string to print
	syscall
	j Exit
	
	.text 0x0042205c
Far:	li $v0, 4 # system call for print_str
	la $a0, msg2 # address of string to print
	syscall

Exit:	addu $ra, $0, $s0 
	jr $ra

