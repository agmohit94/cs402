/*=============================================================================
 | Author:  Mohit Aggarwal
 | Date:  18/Apr/19
 | Assigment: SP Lab 1
 | Description: This code contains the declarations of the I/O system used 
 | 		in the main program
 +-----------------------------------------------------------------------------*/

#ifndef _readfile_h
#define _readfile_h
#include <stdio.h>

FILE *fp;	//File Pointer which will used to point to the input file.

//Struct used to store the information of the employee
struct Employee
{
    char first[64];
    char last[64];
    int id;
    int salary;
};

//This function is used to open the data file provided while starting the program
//Input - String
//Output - 0 if sucessfully opened the file else -1
int open_file(char name[]);

//This function is used to close the data file provided while starting the program
//Input - File Pointer
//Output - 0 if sucessfully closed the file else -1
int close_file(FILE *f);

//This function is used to read integer values from the input  file
//Input - Integer Pointer
//Output - 0 if sucessfully read the input else -1
int read_int(int *a);

//This function is used to read float values from the input  file
//Input - Float Pointer
//Output - 0 if sucessfully read the input else -1
int read_float(float *a);

//This function is used to read String values from the input  file
//Input - Char Pointer
//Output - 0 if sucessfully read the input else -1
int read_string(char *a);

//This function is used to sort the input from file by ID in increasing order
//Input - Array of Employee Struct & size of the array
//Output - N/A
void sortbyID(struct Employee arr[], int n);

#endif
