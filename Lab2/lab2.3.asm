	.data 0x10010000
var1:	.word 2		# var1 is a word (32 bit) with value 2
var2: 	.word 3		# var2 is a word (32 bit) with value 3
var3: 	.word 5		# var3 is a word (32 bit) with value 5
var4: 	.word 8		# var4 is a word (32 bit) with value 8
first: 	.byte 'M'	# first is a byte (4 bit) with value M
last: 	.byte 'A'	# first is a byte (4 bit) with value A
 	.text
	.globl main

main:	addu $s0, $ra, $0 # save $31 in $16
	lui $t0, 4097		  # store in the upper immediate(leftmost) bit of $t0 value of 0x1001
	lw $t1, 0($t0)		  # Load in $t1 the value stored at the address contained in $t0
	lw $t2, 4($t0)		  # Load in $t2 the value stored at the address contained in $t0 + 4
	lw $t3, 8($t0)		  # Load in $t3 the value stored at the address contained in $t0 + 8
	lw $t4, 12($t0)		  # Load in $t4 the value stored at the address contained in $t0 + 12
	sw $t1, 12($t0)		  # Store the value in $t1 at the address $t0 + 12
	sw $t2, 8($t0)		  # Store the value in $t2 at the address $t0 + 8
	sw $t3, 4($t0)		  # Store the value in $t3 at the address $t0 + 4
	sw $t4, 0($t0)		  # Store the value in $t4 at the address $t0

# restore now the return address in $ra and return from main
	addu $ra, $0, $s0 # return address back in $31
	addu $0, $0, $0
	jr $ra	# return from main
