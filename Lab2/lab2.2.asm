	.data 0x10010000
var1:	.word 2		# var1 is a word (32 bit) with value 2
var2: 	.word 3		# var2 is a word (32 bit) with value 3
var3: 	.word 5		# var3 is a word (32 bit) with value 5
var4: 	.word 8		# var4 is a word (32 bit) with value 8
first: 	.byte 'M'	# first is a byte (4 bit) with value M
last: 	.byte 'A'	# first is a byte (4 bit) with value A
 	.text
	.globl main

main:	addu $s0, $ra, $0 # save $31 in $16
		lw $t0, var1	  # load value of var1 in $t0
		lw $t1, var2	  # load value of var2 in $t1
		lw $t2, var3	  # load value of var3 in $t2
		lw $t3, var4	  # load value of var4 in $t3
		sw $t0, var4	  # save value of $t0 in var4
		sw $t1, var3	  # save value of $t1 in var3
		sw $t2, var2	  # save value of $t2 in var2
		sw $t3, var1	  # save value of $t3 in var1

# restore now the return address in $ra and return from main
	addu $ra, $0, $s0 # return address back in $31
	jr $ra	# return from main
