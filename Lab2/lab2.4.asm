	.data 0x10010000
var1:	.word 51		# var1 is a word (32 bit) with value 51
var2: 	.word 28		# var1 is a word (32 bit) with value 28

	.extern ext1 4		# Reserve space(4 byte) for a variable named ext1 
	.extern ext2 4		# Reserve space(4 byte) for a variable named ext2
 	.text
	.globl main

main:	addu $s0, $ra, $0 # save $31 in $16
		lw $t0, var1	  # load value of var1 in $t0
		lw $t1, var2      # load value of var2 in $t1
		sw $t1, ext1      # save value of $t1 in ext1
		sw $t0, ext2      # save value of $t0 in ext2

# restore now the return address in $ra and return from main
	addu $ra, $0, $s0 # return address back in $31
	jr $ra	# return from main
